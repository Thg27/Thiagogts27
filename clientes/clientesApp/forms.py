from django.forms import ModelForm
from  .models import Cliente

class ClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'age', 'salary', 'bio']